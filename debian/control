Source: timblserver
Section: science
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Joost van Baal-Ilić <joostvb@debian.org>,
 Ko van der Sloot <ko.vandersloot@uvt.nl>,
 Maarten van Gompel <proycon@anaproy.nl>,
Build-Depends:
 autoconf-archive,
 dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
 libticcutils-dev (>= 0.36),
 libtimbl-dev (>= 6.10),
 libxml2-dev,
 pkgconf,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://languagemachines.github.io/timbl/
Vcs-Git: https://salsa.debian.org/science-team/timblserver.git
Vcs-Browser: https://salsa.debian.org/science-team/timblserver

Package: timblserver
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Server extensions for Timbl
 timblserver is a TiMBL wrapper; it adds server functionality to TiMBL.  It
 allows TiMBL to run multiple experiments as a TCP server, optionally via HTTP.
 .
 The Tilburg Memory Based Learner, TiMBL, is a tool for Natural Language
 Processing research, and for many other domains where classification tasks are
 learned from examples.
 .
 TimblServer is a product of the ILK Research Group (Tilburg University, The
 Netherlands) and the CLiPS Research Centre (University of Antwerp, Belgium).
 .
 If you do scientific research in NLP, TimblServer will likely be of use to you.

Package: libtimblserver5
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 libtimblserver4
Conflicts:
 libtimblserver4
Description: Server extensions for Timbl - runtime
 timblserver is a TiMBL wrapper; it adds server functionality to TiMBL.  It
 allows TiMBL to run multiple experiments as a TCP server, optionally via HTTP.
 .
 The Tilburg Memory Based Learner, TiMBL, is a tool for Natural Language
 Processing research, and for many other domains where classification tasks are
 learned from examples.
 .
 TimblServer is a product of the ILK Research Group (Tilburg University, The
 Netherlands) and the CLiPS Research Centre (University of Antwerp, Belgium).
 .
 This package provides the runtime files required to run programs that use
 timblserver

Package: libtimblserver-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libtimblserver5 (= ${binary:Version}),
 ${misc:Depends},
Conflicts:
 libtimblserver2-dev,
 libtimblserver3-dev,
 libtimblserver4-dev,
Replaces:
 libtimblserver2-dev,
 libtimblserver3-dev,
 libtimblserver4-dev,
Description: Server extensions for Timbl - development
 timblserver is a TiMBL wrapper; it adds server functionality to TiMBL.  It
 allows TiMBL to run multiple experiments as a TCP server, optionally via HTTP.
 .
 The Tilburg Memory Based Learner, TiMBL, is a tool for Natural Language
 Processing research, and for many other domains where classification tasks are
 learned from examples.
 .
 TimblServer is a product of the ILK Research Group (Tilburg University, The
 Netherlands) and the CLiPS Research Centre (University of Antwerp, Belgium).
 .
 This package provides the header files required to compile C++ programs that
 use timblserver.
